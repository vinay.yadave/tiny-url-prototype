import hashlib
import logging
import base64
import random
import threading
from datetime import datetime

lock = threading.RLock()


class KeyGenerationService:
    logger = logging.getLogger(__name__)
    ID_LENGTH = 12

    def __init__(self):
        pass

    def generate_key(self, url):
        """
        Strategy to generate the 12 character url is
        - appending the worker id and timestamp to the url.
        - compute SHA256 hash of this url.
        - Encode this url with base62
        :param url:
        :return:
        """
        formed_url = self.__get_formed_url(url)
        tiny_key = self.__encode_base64(formed_url)
        return tiny_key

    @staticmethod
    def __get_hash_sha256(url: str):
        hash_256 = hashlib.sha256(url.encode("utf-8"))
        hash_string = hash_256.hexdigest()
        KeyGenerationService.logger.info(hash_string)
        return hash_string

    @staticmethod
    def __get_worker_id():
        worker_id: str = ""
        num_workers = random.randrange(20)
        with lock:
            workers_list = random.sample(range(0, num_workers + 1), num_workers)
            worker_id = str(random.choice(workers_list))
        return str(worker_id)

    @staticmethod
    def __get_timestamp():
        return str(datetime.now().timestamp()).split(".")[0]

    def __get_formed_url(self, url: str):
        timestamp: str = self.__get_timestamp()
        worker_id: str = self.__get_worker_id()
        url = worker_id + url + timestamp
        hashed_url = self.__get_hash_sha256(url)
        return hashed_url

    def __encode_base64(self, url: str):
        url_bytes = url.encode("ascii")
        encoded_bytes = base64.urlsafe_b64encode(url_bytes)
        encoded_str = encoded_bytes.decode("ascii")
        tiny_url: str = ""
        for i in range(0, self.ID_LENGTH+1):
            index = random.randrange(0, len(encoded_str))
            ch = encoded_str[index]
            tiny_url = tiny_url+encoded_str[index]
        return tiny_url.rstrip()
