import logging

from injector import inject
from pymodm.errors import ValidationError
from pymongo.errors import ConnectionFailure

from model.MappingModel import Mapping
from service.KeyGenerationService import KeyGenerationService
from utility import Log


class UrlService:

    @inject
    def __init__(self):
        self.logger = Log(__name__)
        self.placeholder = "https://google.com/"

    def get_long_url(self, tiny_url):
        try:
            self.logger.log(f"id - {tiny_url}")
            mapping = Mapping.objects.get({'_id': tiny_url})
            self.logger.log(f"long url - {mapping.url}")
            return mapping.url, mapping
        except Mapping.DoesNotExist:
            raise ValueError("No URL")

    def get_tiny_url(self, long_url: str):
        self.logger.log(f"Request - {long_url}")
        mapping_entry: any = None
        try:
            tiny_id = KeyGenerationService().generate_key(long_url)
            mapping_entry = Mapping(id=tiny_id, url=long_url).save()
        except (ValidationError, ConnectionFailure):
            mapping_entry = None

        return mapping_entry
