import logging
from flask import Flask, jsonify, request, url_for, redirect
from flask_api import FlaskAPI, status, exceptions
from flask_expects_json import expects_json
import werkzeug.exceptions as ex

from model.MappingModel import Mapping, schema
from service.UrlService import UrlService
from utility import Log
from utility.HouseKeeping import HouseKeeping, valid_url
from utility.Response import AppResponse

app = Flask(__name__)
HouseKeeping.clean_up()

url_service = UrlService()
logger = Log(__name__)


@app.errorhandler(ex.NotFound)
def resource_not_found(error):
    return jsonify(message="resource not found"), status.HTTP_404_NOT_FOUND


@app.errorhandler(ex.BadRequest)
def handle_bad_request(error):
    return jsonify(message="Invalid request"), status.HTTP_400_BAD_REQUEST


@app.route("/api/generateTinyUrl", methods=["POST"])
@expects_json(schema)
def generate_tiny_url():
    """
    API to generate tiny (short) url. Gets the long url from user.
    Validates the input URL, if valid generates short url and return
    :return: tiny (short) url
    """
    url_self_link = HouseKeeping.get_url(request, "generate_tiny_url")
    logger.log(url_self_link)

    # get the request data, i.e url
    long_url = request.get_json().get("url")
    logger.log(long_url)

    if not valid_url(long_url):
        resp = AppResponse(content=None, link=url_self_link, message="invalid url", success=False)
        raise ex.BadRequest()

    # else send the url to service to process
    logger.log("Invoking service ....")
    mapping: Mapping = url_service.get_tiny_url(long_url)

    json_dict = mapping.json()
    json_dict["shortenUrl"] = HouseKeeping.get_base_url(request) + "/" + json_dict["id"]

    resp = AppResponse(content=json_dict, link=url_self_link)
    logger.log(resp.content)
    return resp.build(), status.HTTP_200_OK


@app.route("/<tiny_id>", methods=["GET"])
def load_url(tiny_id):
    """
    Function to load original url up on receiving valid tiny url
    if tiny url is not valid, return bad req
    :param tiny_id:
    :return:
    """
    logger.log(f"id-{tiny_id}")
    long_url = ""

    try:
        long_url, _ = url_service.get_long_url(tiny_id)
    except ValueError:
        raise ex.NotFound()
    return redirect(long_url), status.HTTP_301_MOVED_PERMANENTLY


@app.route("/api/getLongUrl", methods=["GET"])
def get_long_url():
    """
    This function takes valid tiny url return corresponding long url.
    if tiny url is not valid - bad req
    if no long url - bad req
    :return:
    """
    # get the req
    # validate the req
    if "tinyUrl" in request.args.to_dict(flat=True):
        tiny_url = request.args.get("tinyUrl")
        url_self = HouseKeeping.get_base_url(request) + url_for("get_long_url")
        long_url, mapping = url_service.get_long_url(tiny_url)
        resp = AppResponse(content=mapping.json(), link=url_self)
        return resp.build(), status.HTTP_200_OK
    else:
        raise ex.NotFound()


if __name__ == '__main__':
    app.run(load_dotenv=True, debug=True)
