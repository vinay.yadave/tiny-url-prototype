import logging


class Log:
    def __init__(self, name: str, level: int = logging.INFO):
        self.__name: str = name
        self.__level: int = level
        self.__logger = logging.getLogger(self.__name)
        self.__logger.setLevel(level)
        self.__logger.propagate = True
        self.__console_handler = logging.StreamHandler()
        self.__console_handler.setLevel(level)
        self.__formatter = logging.Formatter(fmt="%(asctime)s - %(name)s - %(levelname)s %(message)s")
        self.__console_handler.setFormatter(self.__formatter)
        self.__logger.addHandler(self.__console_handler)

    def log(self, message: str):

        if self.__level == logging.INFO:
            self.__logger.info(message)
        elif self.__level == logging.DEBUG:
            self.__logger.debug(message)
        elif self.__level == logging.ERROR:
            self.__logger.error(message)
        elif self.__level == logging.CRITICAL:
            self.__logger.critical(message)
        elif self.__level == logging.WARN:
            self.__logger.warning(message)
        else:
            self.__logger.fatal(message)


