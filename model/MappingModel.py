from pymodm import MongoModel, fields
from pymongo import WriteConcern

from model.Database import MongoDb

alias = MongoDb().connect()


class Mapping(MongoModel):

    id = fields.CharField(primary_key=True)
    url = fields.URLField(required=True)

    class Meta:
        write_concern = WriteConcern(j=True)
        connection_alias = alias

    def json(self):
        json_dict = {
            "id": self.id,
            "longUrl": self.url
        }
        return json_dict


schema = {
    'type': 'object',
    'properties': {
        'id': {'type': 'string'},
        'url': {'type': 'string'}
    },
    'required': ['url']
}
