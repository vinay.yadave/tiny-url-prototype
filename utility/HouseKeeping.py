import requests
from pymongo import MongoClient
from flask import request, url_for
import logging

from urlvalidator import URLValidator, ValidationError

from config.Util import Util

logger = logging.getLogger(__name__)


class HouseKeeping:
    _mongo_url, _db_alias = Util.get_db_info(config_name="mongodb.dev")

    @classmethod
    def set_up(cls):
        pass

    @classmethod
    def clean_up(cls):
        mongo_client = MongoClient(cls._mongo_url)
        db_list = mongo_client.database_names()
        if cls._db_alias in db_list:
            logger.info("Deleting " + cls._db_alias)
            mongo_client.drop_database(cls._db_alias)
        mongo_client.close()

    @staticmethod
    def get_url(req: request, path: str):
        url = req.host_url.rstrip('/') + url_for(path)
        return url

    @staticmethod
    def get_base_url(req: request):
        base_url = req.host_url.rstrip('/')
        return base_url


def valid_url(url: str):
    flag = False

    try:
        URLValidator(url)
        flag = True
    except ValidationError as exception:
        flag = False
    finally:
        return flag



