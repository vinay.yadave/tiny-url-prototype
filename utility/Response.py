from flask import jsonify


class AppResponse:
    def __init__(self, content: any, link: str, message: str = None, success: bool = True):
        """
        :param content:
        :param link:
        :param message:
        :param success:
        :return:
        """
        self.content = content
        self.link = link
        self.message = message
        self.success = success

    def build(self):
        self_link = self.__preparelink()

        if self.message and not self.success:
            return jsonify(message=self.message, success=self.success, _link=self_link)
        elif self.message and self.success:
            return jsonify(content=self.content, message=self.message, _link=self_link)
        else:
            return jsonify(content=self.content, _link=self_link)

    def __preparelink(self):
        link_dict = {
            "self": {
                "href": self.link
            }
        }
        return link_dict
