from abc import ABC, abstractmethod
from pymodm import connect
import logging

from config.Util import Util

logger = logging.getLogger(__name__)


class UrlDatabase(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def connect(self):
        pass


class MongoDb(UrlDatabase):
    def __init__(self):
        super().__init__()

    def connect(self):
        mongodb_uri, db_alias = Util.get_db_info("mongodb.dev")
        try:
            connect(mongodb_uri, db_alias)
            logger.info("Connected to MongoDB successfully.")
        except RuntimeError:
            logger.error("Failed to connect to MongoDB.")
        return db_alias
