from injector import singleton
from model.MappingModel import Mapping
from service.KeyGenerationService import KeyGenerationService
from service.UrlService import UrlService


def configure(binder):
    binder.bind(Mapping, to=Mapping, scope=singleton)
    binder.bind(KeyGenerationService, to=KeyGenerationService, scope=singleton)
    binder.bind(UrlService, to=UrlService, scope=singleton)
